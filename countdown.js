// Default configuration that can be overriden
// by url querystring parameters
let secondsCount = 7200;
let title = '';
let urlRedirection = '';

// Other variables
let timer;
let stylesheet = document.styleSheets[0];


function init() {
    try {
        refillConfigurationForm();
        analyseQueryString();

        if(title?.trim()) {
            console.log('title has at least one non-whitespace character.')
            document.getElementById('countdown-title').textContent = title;
        }

        timer = setInterval('countdown()',1000);

        // live configuration
        document.getElementById('form-title').onchange = liveTitle;
        document.getElementById('form-seconds').onchange = liveSeconds;
        document.getElementById('form-text-color').onchange = liveTextColor;
        document.getElementById('form-background-color').onchange = liveBackgroundColor;
        document.getElementById('show-configuration').onclick = toggleConfiguration;
    }
    catch (error)
    {
        console.log('Unable to initialize counter: ' + error);
    }

}

function liveTitle() {
    document.getElementById('countdown-title').textContent = document.getElementById('form-title').value;
    console.log('[live] Countdown title update to [' + document.getElementById('form-title').value + ']');
}

function liveSeconds() {
    secondsCount = document.getElementById('form-seconds').value;
    console.log('[live] Countdown duration update to [' + secondsCount + '] seconds');
}

function liveTextColor() {
    document.getElementById('countdown-section').style.color = document.getElementById('form-text-color').value;
    console.log('[live] Countdown text color update to [' + document.getElementById('form-text-color').value + ']');
}

function liveBackgroundColor() {
    stylesheet.cssRules[0].style.backgroundColor = document.getElementById('form-background-color').value;
    console.log('[live] Countdown background color update to [' + document.getElementById('form-background-color').value + ']');
}

function toggleConfiguration() {
    console.log('Toggle main title visibility');
    document.getElementById('main-title').classList.toggle("visible");
    document.getElementById('main-title').classList.toggle("hidden");

    console.log('Toggle configuration form visibility');
    document.getElementById('configuration-form').classList.toggle("visible");
    document.getElementById('configuration-form').classList.toggle("hidden");
}

function refillConfigurationForm() {
    // Querystring parameters:
    // s: secondsCount
    // t: title
    // r: urlRedirection
    // c: textColor
    // b: backgroundColor

    const queryString = window.location.search;
    const urlParams = new URLSearchParams(queryString);

    if (urlParams.has("s") === true) {
        //toggleConfiguration;
        document.getElementById('form-seconds').value = urlParams.get("s");
    }

    if (urlParams.has("t") === true) {
        document.getElementById('form-title').value = urlParams.get("t");
    }

    if (urlParams.has("b") === true) {
        document.getElementById('form-background-color').value = urlParams.get("b");
    }

    if (urlParams.has("c") === true) {
        document.getElementById('form-text-color').value = urlParams.get("c");
    }
}

function analyseQueryString() {
    // Querystring parameters:
    // s: secondsCount
    // t: form-title
    // r: urlRedirection
    // c: textColor
    // b: backgroundColor

    const queryString = window.location.search;
    const urlParams = new URLSearchParams(queryString);

    if (urlParams.has("s") === true) {
        paramSecondsCount = parseInt(urlParams.get("s"));
        if (Number.isInteger(paramSecondsCount)) {
            // document.getElementById('configuration-form').style.display = 'none';
            console.log('[querystring] Countdown duration update to [' + paramSecondsCount + ']');
            secondsCount = paramSecondsCount + 1;

            console.log('Toggle configuration form visibility');
            document.getElementById('configuration-form').classList.toggle("visible");
            document.getElementById('configuration-form').classList.toggle("hidden");

            console.log('Toggle main title visibility');
            document.getElementById('main-title').classList.toggle("visible");
            document.getElementById('main-title').classList.toggle("hidden");
        }
    }

    if (urlParams.has("t") === true) {
        title = urlParams.get("t");
        console.log('[querystring] Countdown title update to [' + title + ']');
    }

    if (urlParams.has("b") === true) {
        // TODO: check color validity
        stylesheet.cssRules[0].style.backgroundColor = urlParams.get("b");
        console.log('[querystring] Countdown background color update to [' + urlParams.get("b") + ']');
    }

    if (urlParams.has("c") === true) {
        // TODO: check color validity
        document.getElementById('countdown-section').style.color = urlParams.get("c");
        console.log('[querystring] Countdown text color update to [' + urlParams.get("c") + ']');
    }
}

function countdown() {
    secondsCount-- ;
    d = parseInt((secondsCount / 86400));
    h = parseInt((secondsCount % 86400) / 3600);
    m = parseInt((secondsCount % 3600) / 60);
    s = parseInt((secondsCount % 3600) % 60);

    countdownElement = document.getElementById('countdown');
    countdownElement.innerHTML = '';
    if (d > 0) countdownElement.innerHTML += '<div>' + (d < 10 ? "0" + d : d) + ' <span class="unity">jours</span></div>';
    countdownElement.innerHTML += '<div>' + (h < 10 ? "0" + h : h) + ' <span class="unity">heures</span></div>';
    countdownElement.innerHTML += '<div>' + (m < 10 ? "0" + m : m) + ' <span class="unity">minutes</span></div>';
    countdownElement.innerHTML += '<div>' + (s < 10 ? "0" + s : s) + ' <span class="unity">secondes</span></div>';

    if (s == 0 && m == 0 && h == 0 && d == 0) { // if counter reach 0
        clearInterval(timer);
        if (urlRedirection?.trim()) { // if urlRedirector has at least one non-whitespace character
            redirection(url);
        }
    }
}

function redirection(url) {
    setTimeout("window.location=url", 500);
}


// Event listener depending of the browser
function addListener(element, baseName, handler) {
    if (element.addEventListener)
        element.addEventListener(baseName, handler, false);
    else if (element.attachEvent)
        element.attachEvent('on' + baseName, handler);
} // addListener


// First action when the page is loaded
addListener(window, 'load', init);
